Rails.application.routes.draw do
    devise_for :users
    root to: 'welcome#index'
    get 'thread/index'
    get 'thread/new'
    post 'thread/new'
    post 'thread/show'
    match 'thread/show?id=:id' => 'photos#show', :via => [:get, :post]
    get 'thread/show'
    get 'thread/create'
    post 'thread/create'
    get 'thread/view'
    post 'thread/write'
    get 'thread/write'
    get 'thread/seed'
    get "welcome/index"
    get "welcome", to: "welcome#index", as: :welcome 
    resources :activities 
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
