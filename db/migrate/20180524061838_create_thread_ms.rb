class CreateThreadMs < ActiveRecord::Migration[5.2]
  def change
    create_table :thread_ms do |t|
      t.string :title
      t.string :username
      t.text :body

      t.timestamps
    end
  end
end
