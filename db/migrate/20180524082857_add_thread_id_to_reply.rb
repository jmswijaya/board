class AddThreadIdToReply < ActiveRecord::Migration[5.2]
  def change
    add_column :reply_ms, :thread_m_id, :integer
  end
end
