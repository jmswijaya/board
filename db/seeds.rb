# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
Role.destroy_all

r1 = Role.create(:name => 'user')  
r2 = Role.create(:name => 'admin')  

p "Created #{Role.count} roles."

us1 = User.create(:email => 'user@gmail.com' , :password => 'user_123', :role_id => r1.id)   
us2 = User.create(:email => 'admin@gmail.com' , :password => 'admin_123', :role_id => r2.id)

p "Created #{User.count} roles."

ReplyM.destroy_all
ThreadM.destroy_all

t1 = ThreadM.create(:title => 'Marker', :username => 'wijaya-m', :body => "Hello Mello ...")
t2 = ThreadM.create(:title => 'Spam Marker', :username => 'vionita-j', :body => "Hello Mello 123 ...")

p "Created #{ThreadM.count} threads."

r1 = ReplyM.create(:username => 'vionita-m', :body => "Reply Hello Mello ...", :thread_m_id => t1.id)
r2 = ReplyM.create(:username => 'wijaya-j', :body => "Reply 123", :thread_m_id => t2.id)

if ReplyM.count > 1
	p "Created #{ReplyM.count} replies."
else
	p "Created #{ReplyM.count} replys."
end