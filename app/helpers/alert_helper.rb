module AlertHelper
	def showError(passVar)
		res = ""
		if passVar.errors.full_messages.any?
			list_err = ""
			passVar.errors.full_messages.each do |error_message|
				list_err = list_err + "<li>" + error_message + "</li>"
			end

			res =   "<div class='alert alert-error'>" +
						"<ul>" +
							list_err +
						"</ul>" +
					"</div>"
		end

		res.html_safe
	end
end