class ThreadController < ApplicationController
	include AlertHelper

	def param_reply
		params.require(:reply_m).permit(:username, :body)
	end

	def param_thread
		params.require(:thread_m).permit(:title, :username, :body)
	end

	def index
		@threads = ThreadM.all
	end

	def seed
		thread = ThreadM.new do |t|
					t.title = "David"
					t.username = "Code Artist"
					t.body = "Hay Hay Hay"
				end
		if thread.save
	    	redirect_to '/'
	    end
	end

	def new
		if request.post?
			@thread = ThreadM.create(param_thread)

			if @thread.valid?
				id_thread = @thread.id
				flash[:success] = "Works! New Thread has created."
    			#string interpolation = "Hello, #{name}!"
    			redirect_to "/thread/show?id=#{id_thread}"
			else
				#--OPTION 1--
				render "thread/new"
				#--OPTION 2--
				#flash[:error] = "Something Wrong! Failed to create new thread."
				#redirect_to "/thread/new"
			end
		else
			@thread = ThreadM.new	
		end
	end

	def create
		#https://www.ruby-forum.com/topic/2378990
		#@trd = ThreadM.new
		if request.post?
			#@product = Product.new({:serial => params[:serial], :value => params[:value]})
			#new_thread = params.require(:thread).permit(:title, :username, :body)
			thread = ThreadM.new do |t|
						t.title = params[:title]
						t.username = params[:username]
						t.body = params[:body]
					end
	    	#thread = ThreadM.create(new_thread)
	    	if thread.save
		    	redirect_to '/'
		    end
		end
	end

	def view
		id_thread = params[:id]
		@cur_thread = ThreadM.find_by(id: id_thread)
		@thread_reply = ReplyM.where(:thread_m_id => id_thread)
	end

	def show
		id_thread = params[:id]
		@cur_thread = ThreadM.find_by(id: id_thread)
		@thread_reply = ReplyM.where(:thread_m_id => id_thread)
		
		if request.post?
			thread = ThreadM.find_by(id: id_thread)
			if thread.valid?
				temp_com = param_reply
				temp_com[:thread_m_id] = thread.id
				@reply = thread.reply_ms.create(temp_com)
				if @reply.valid?
					id_thread = thread.id
					flash[:success] = "Works! New Comment has added."
    				redirect_to "/thread/show?id=#{id_thread}"
				else
					render "thread/show"
				end
			end
		else
			if id_thread
				@reply = ReplyM.new
			else 
				redirect_to "/"
			end
		end
	end

	def write
		if request.post?
			id_thread = params[:thread_id]
			thread = ThreadM.find_by(id: id_thread)

			if thread.valid?
				#p thread.id
				reply = thread.reply_ms.create(
							username: params[:username], 
							body: params[:body], 
							thread_m_id: thread.id
						)

		    	if reply.valid?
		    		p "Created #{ReplyM.count} reply(s)" 
		    		if reply.save
				    	redirect_to '/thread/view?id=' + thread.id.to_s 
				    end
		    	else
		    		p reply.errors
		    	end
			end
		else
			redirect_to '/'
		end
		
	end
end
