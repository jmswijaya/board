class ReplyM < ApplicationRecord
	#belongs_to :threadm
	belongs_to :thread_m
	validates :username, length: { in: 1..16 }
	validates :body, length: { in: 1..200 }
	include PublicActivity::Model  
  		tracked owner: ->(controller, model) { controller && controller.current_user}
end