class ThreadM < ApplicationRecord
    include PublicActivity::Model  
  		tracked owner: ->(controller, model) { controller && controller.current_user}
	has_many :reply_ms
	validates :title, length: { minimum: 2  }
	validates :username, length: { in: 1..16 }
	validates :body, length: { in: 1..200 }
	
end
